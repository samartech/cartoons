package com.zeroone.haris.cartoons;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Window;
import android.webkit.WebView;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class VideoActivity3 extends YouTubeBaseActivity {

    private RecyclerView recyclerView;
    private ArrayList<YoutubeVideo> youtubeVideos = new ArrayList<YoutubeVideo>();
    private FirebaseDatabase db;
    private DatabaseReference dbRef;
    private DatabaseReference keyRef;
    private EpisodeListAdapter videoAdapter;
    private YouTubePlayerView videoView;
    private YouTubePlayer.OnInitializedListener onInitializedListener;
    public YouTubePlayer mYoutubePlayer;
    private String key;



    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        setContentView(R.layout.activity_video3);


        recyclerView = (RecyclerView) findViewById(R.id.episodeRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager( new LinearLayoutManager(this));
        db = FirebaseDatabase.getInstance();
        final String cartoonName = getIntent().getExtras().getString("name");
        dbRef = db.getReference().child("Cartoons").child(cartoonName).child("episodes");
        videoView = findViewById(R.id.videoView);

        keyRef = db.getReference().child("YoutubeKey");
        keyRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                onInitializedListener = new YouTubePlayer.OnInitializedListener() {
                    @Override
                    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {

                        if(!b)
                            youTubePlayer.cueVideo(youtubeVideos.get(0).getID());
                        mYoutubePlayer = youTubePlayer;

                    }

                    @Override
                    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

                    }
                };
                key = (String)dataSnapshot.getValue();
                videoView.initialize(key, onInitializedListener);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



        //fetch videos array from firebase DB
        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();

                for (DataSnapshot child:children){
                    youtubeVideos.add(child.getValue(YoutubeVideo.class));
                }



                //String playVideo= "<html><body> <iframe class=\"youtube-player\" type=\"text/html\" width=\"640\" height=\"385\" src=\""+youtubeVideos.get(0).getVideoUrl()+"\" frameborder=\"0\"></body></html>";
                //videoView.initialize(key, onInitializedListener);
                videoAdapter= new EpisodeListAdapter(youtubeVideos, VideoActivity3.this);
                recyclerView.setAdapter(videoAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    /*@Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState )
    {
        super.onSaveInstanceState(outState);
        videoView.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
        videoView.restoreState(savedInstanceState);
    }*/

    /*@Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }*/
}
