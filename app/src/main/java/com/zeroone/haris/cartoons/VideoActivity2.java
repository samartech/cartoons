package com.zeroone.haris.cartoons;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class VideoActivity2 extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<YoutubeVideo> youtubeVideos = new ArrayList<YoutubeVideo>();
    private FirebaseDatabase db;
    private DatabaseReference dbRef;
    EpisodeListAdapter videoAdapter;
    WebView videoView;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        setContentView(R.layout.activity_video2);
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);   //show back button


        recyclerView = (RecyclerView) findViewById(R.id.episodeRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager( new LinearLayoutManager(this));
        db = FirebaseDatabase.getInstance();
        final String cartoonName = getIntent().getExtras().getString("name");
        dbRef = db.getReference().child("Cartoons").child(cartoonName).child("episodes");
        videoView = findViewById(R.id.videoView);
        videoView.setWebViewClient(new WebViewClientCustom());
        videoView.setWebChromeClient(new WebChromeClientCustom());
        videoView.getSettings().setJavaScriptEnabled(true);
        videoView.getSettings().setAllowFileAccess(true);
        videoView.getSettings().setAppCacheEnabled(true);

        //TODO: fetch videos array from firebase DB
        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();

                for (DataSnapshot child:children){
                    youtubeVideos.add(child.getValue(YoutubeVideo.class));
                }

                //String playVideo= "<html><body> <iframe class=\"youtube-player\" type=\"text/html\" width=\"640\" height=\"385\" src=\""+youtubeVideos.get(0).getVideoUrl()+"\" frameborder=\"0\"></body></html>";
                if(savedInstanceState==null) {
                    videoView.loadData("<html><body><iframe width=\"99%\" height=\"315\" src=\"" + youtubeVideos.get(0).getVideoUrl() + "?autoplay=1&showinfo=0&controls=1\" allowfullscreen=\"allowfullscreen\" mozallowfullscreen=\"mozallowfullscreen\" msallowfullscreen=\"msallowfullscreen\" oallowfullscreen=\"oallowfullscreen\" webkitallowfullscreen=\"webkitallowfullscreen\"  </iframe></body></html>",
                            "text/html", "utf-8");
                }
                videoAdapter= new EpisodeListAdapter(youtubeVideos, VideoActivity2.this);
                recyclerView.setAdapter(videoAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState )
    {
        super.onSaveInstanceState(outState);
        videoView.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
        videoView.restoreState(savedInstanceState);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    class WebViewClientCustom extends WebViewClient {

        WebViewClientCustom() {
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

        }
    }

    private class WebChromeClientCustom extends WebChromeClient {

        private View mCustomView;
        private WebChromeClient.CustomViewCallback mCustomViewCallback;
        protected FrameLayout mFullscreenContainer;
        private int mOriginalOrientation;
        private int mOriginalSystemUiVisibility;

        WebChromeClientCustom() {}

        public Bitmap getDefaultVideoPoster()
        {
            if (mCustomView == null) {
                return null;
            }
            return BitmapFactory.decodeResource(getApplicationContext().getResources(), 2130837573);
        }

        public void onHideCustomView()
        {
            ((FrameLayout)getWindow().getDecorView()).removeView(this.mCustomView);
            this.mCustomView = null;
            getWindow().getDecorView().setSystemUiVisibility(this.mOriginalSystemUiVisibility);
            setRequestedOrientation(this.mOriginalOrientation);
            this.mCustomViewCallback.onCustomViewHidden();
            this.mCustomViewCallback = null;
        }

        public void onShowCustomView(View paramView, WebChromeClient.CustomViewCallback paramCustomViewCallback)
        {
            if (this.mCustomView != null)
            {
                onHideCustomView();
                return;
            }
            this.mCustomView = paramView;
            this.mOriginalSystemUiVisibility = getWindow().getDecorView().getSystemUiVisibility();
            this.mOriginalOrientation = getRequestedOrientation();
            this.mCustomViewCallback = paramCustomViewCallback;
            ((FrameLayout)getWindow().getDecorView()).addView(this.mCustomView, new FrameLayout.LayoutParams(-1, -1));
            getWindow().getDecorView().setSystemUiVisibility(3846);
        }
    }
}

