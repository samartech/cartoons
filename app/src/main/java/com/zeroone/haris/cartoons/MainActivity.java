package com.zeroone.haris.cartoons;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private FirebaseDatabase db;
    private DatabaseReference dbRef;
    private ArrayList <Cartoon> fetchedcartoons;
    private ListAdapter adapter;
    private RecyclerView cartoonRecycler;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);

        db = FirebaseDatabase.getInstance();
        dbRef = db.getReference().child("Cartoons");

        fetchedcartoons = new ArrayList<>();

        cartoonRecycler = findViewById(R.id.cartoonRecycler);
        cartoonRecycler.setLayoutManager(new LinearLayoutManager(this));

        progressBar = findViewById(R.id.progressBar);

//        //Create cartoons and add episodes
//        Cartoon doraemon = new Cartoon("Doraemon");
//        doraemon.getEpisodes().add(new Episode("ep1","www.youtube.com"));
//
//        //Add cartoon objects to map
//        final Map<String, Cartoon> cartoons = new HashMap<>();
//        cartoons.put("Doraemon", doraemon);
//        dbRef.setValue(cartoons);

        /*Cartoon jaan = new Cartoon("Jaan","https://firebasestorage.googleapis.com/v0/b/cartoons-5637f.appspot.com/o/hqdefault.jpg?alt=media&token=be646f1e-e93f-4bef-a6d2-5950c4341fcf");
        jaan.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/OeT8XW8WTDg"));
        jaan.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/T8hbQHGFMK4"));
        jaan.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/KF1OBM9rgnw"));
        jaan.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/0BiHnLRZq84"));
        jaan.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/iBvp8obwHpo"));
        jaan.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/NqqCjWAvT3s"));
        jaan.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/O5gme-jMOBY"));
        jaan.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/8L3oZuQh7Y0"));
        jaan.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/w1ZiNg_ZAks"));
        jaan.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/WmO6WfRzKO0"));

        Cartoon quaidSayBaatein = new Cartoon("Quaid Say Baatein","https://firebasestorage.googleapis.com/v0/b/cartoons-5637f.appspot.com/o/quaidsaybaatein.jpg?alt=media&token=f14288e7-5af1-4d9f-8e3a-af6b7bfa400b");
        quaidSayBaatein.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/deCjK0or7GI"));
        quaidSayBaatein.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/0Lzz9m8e0ak"));
        quaidSayBaatein.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/O3LziCv4KJU"));
        quaidSayBaatein.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/1PvTTkHQF1M"));
        quaidSayBaatein.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/i2ARtPtUnaY"));
        quaidSayBaatein.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/xmrwiRI6ufo"));
        quaidSayBaatein.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/S9AFNWXW6d4"));
        quaidSayBaatein.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/oClemCeb5Ks"));
        quaidSayBaatein.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/H8wwVc70UBU"));
        quaidSayBaatein.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/FqrglUdWARY"));
        quaidSayBaatein.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/1xe6HI0M7KE"));

        Cartoon burkaAvenger = new Cartoon("Burka Avenger","https://firebasestorage.googleapis.com/v0/b/cartoons-5637f.appspot.com/o/burkaavenger.jpg?alt=media&token=741dce67-04cb-4efe-958b-bf403b633989");
        burkaAvenger.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/XahbqLdCVhE"));
        burkaAvenger.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/tHwHayZZFiw"));
        burkaAvenger.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/y5Jmvu2WGr8"));
        burkaAvenger.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/tADpDfca2cA"));
        burkaAvenger.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/Xs76QRLrduI"));
        burkaAvenger.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/pr_7lFI48nk"));
        burkaAvenger.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/h3JZGLNYwQ8"));
        burkaAvenger.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/Yc6E0eD6yS4"));
        burkaAvenger.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/tI5NYtnp9NQ"));
        burkaAvenger.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/2YzjlMwyk-s"));

        Cartoon weBareBears = new Cartoon("We Bare Bears","https://firebasestorage.googleapis.com/v0/b/cartoons-5637f.appspot.com/o/webarebears.jpg?alt=media&token=f63e5317-58cb-42e8-ae44-b791418dc027");
        weBareBears.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/hVpVK9pUF9A"));
        weBareBears.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/HTN4PCSpxX0"));
        weBareBears.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/ebN5Dw76D9Y"));
        weBareBears.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/7-gMCcbGDRo"));
        weBareBears.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/OwIyFD0lAxA"));
        weBareBears.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/O7nwdE85lDA"));
        weBareBears.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/Ba746NxDlyg"));
        weBareBears.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/6yRuhCo6BHk"));
        weBareBears.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/gQQzgzdlXPA"));
        weBareBears.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/vTwg8CCENRo"));

        Cartoon milkateers = new Cartoon("Milkateer","https://firebasestorage.googleapis.com/v0/b/cartoons-5637f.appspot.com/o/milkateer.jpg?alt=media&token=b14b5b56-50a6-486c-ad14-6b54dc67ee8b");
        milkateers.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/d46-g0JhN04"));
        milkateers.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/ZdAI3PuOCdU"));
        milkateers.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/Q-P7fjILRBE"));
        milkateers.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/Pq841loxP3k"));
        milkateers.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/ndgWLkpuZA8"));
        milkateers.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/dKkZ3Bu2URM"));

        Cartoon gumball = new Cartoon("The Amazing World of Gumball","https://firebasestorage.googleapis.com/v0/b/cartoons-5637f.appspot.com/o/gumball.jpg?alt=media&token=0e27548b-b9b4-4bce-81d6-7dd6308e8374");
        gumball.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/4C4P77xgyJs"));
        gumball.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/QOzbAPSQj5U"));
        gumball.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/WzMSSBR_j-c"));
        gumball.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/3aKeGWatB1c"));
        gumball.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/q1RHEHImRG8"));
        gumball.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/TYnnI_X8FlE"));
        gumball.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/R8wCxbN7Ms4"));
        gumball.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/lUQNdzgxtME"));
        gumball.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/3J_7h28YaZE"));
        gumball.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/uisv4uE1Wlc"));

        Cartoon winxclub = new Cartoon("Winx Club","https://firebasestorage.googleapis.com/v0/b/cartoons-5637f.appspot.com/o/winxclub.jpg?alt=media&token=bddaaa4b-e426-4350-b9dc-2f90780b6c8f");
        winxclub.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/_ENiRpspMhw"));
        winxclub.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/1IsDvZTjCWU"));
        winxclub.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/gQhbXnA2bq8"));
        winxclub.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/ZJYwo9v9UVA"));
        winxclub.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/qe4BQjS-Qys"));
        winxclub.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/bx1iFEk50jQ"));
        winxclub.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/2T_HvAkYh5M"));
        winxclub.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/tV8gpAQhljk"));
        winxclub.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/1_9sFSptURU"));
        winxclub.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/oyOIqeVjjjI"));

        Cartoon mrbean = new Cartoon("Mr Bean Animated","https://firebasestorage.googleapis.com/v0/b/cartoons-5637f.appspot.com/o/mrbean.jpg?alt=media&token=5ba06fbc-0e08-4531-b5c1-44fe820e043a");
        mrbean.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/D0ZMmQjKn2Q"));
        mrbean.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/ali5aVnZ5h8"));
        mrbean.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/SWAa15D7dW8"));
        mrbean.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/00ayLtcmoC8"));
        mrbean.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/3pI_3NZNU8M"));
        mrbean.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/jyKbZ2V1MG4"));
        mrbean.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/bhuKGm7CGHk"));
        mrbean.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/vOXFBu9i7bg"));
        mrbean.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/e0L1vHJkvoQ"));
        mrbean.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/noT4V-LGT-I"));

        Cartoon motupatlu = new Cartoon("Motu Patlu","https://firebasestorage.googleapis.com/v0/b/cartoons-5637f.appspot.com/o/motupatlu.jpg?alt=media&token=7001718b-55e2-425b-958f-61d2cdc04d58");
        motupatlu.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/4of37kzfX7w"));
        motupatlu.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/UmKbycPy3cw"));
        motupatlu.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/u9w7zMylaJI"));
        motupatlu.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/o30988gJDPg"));
        motupatlu.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/opFzpKJQUaE"));
        motupatlu.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/Qc3aC5QHe8Y"));
        motupatlu.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/pMttm2KXYj4"));
        motupatlu.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/bv8ZQqKiM7I"));
        motupatlu.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/FX6Ac6E7PLE"));
        motupatlu.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/wA5kHxz3SDI"));

        Cartoon teenTitans = new Cartoon("Teen Titans Go!","https://firebasestorage.googleapis.com/v0/b/cartoons-5637f.appspot.com/o/teentitans.jpg?alt=media&token=7087a10d-38d4-470d-955a-58b6c5a20a1b");
        teenTitans.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/705vTy4lNuI"));
        teenTitans.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/8cXQNZa6nwM"));
        teenTitans.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/_CH25lGGk48"));
        teenTitans.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/14naP6JOH_c"));
        teenTitans.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/WkpZKfgBKxk"));
        teenTitans.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/4RByS_Yv9iA"));
        teenTitans.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/10davb_vV_8"));
        teenTitans.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/tO_yAaMfaZ8"));
        teenTitans.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/Ob_TrGgnyfk"));
        teenTitans.getEpisodes().add(new YoutubeVideo("https://www.youtube.com/embed/X5JYjIqY2CA"));

        //Add cartoon objects to map
        final Map<String, Cartoon> cartoons = new HashMap<>();
        cartoons.put(jaan.getName(),jaan);
        cartoons.put(quaidSayBaatein.getName(),quaidSayBaatein);
        cartoons.put(burkaAvenger.getName(),burkaAvenger);
        cartoons.put(weBareBears.getName(),weBareBears);
        cartoons.put(milkateers.getName(),milkateers);
        cartoons.put(gumball.getName(),gumball);
        cartoons.put(winxclub.getName(),winxclub);
        cartoons.put(mrbean.getName(),mrbean);
        cartoons.put(motupatlu.getName(),motupatlu);
        cartoons.put(teenTitans.getName(),teenTitans);

        dbRef.setValue(cartoons);*/

        //getting all cartoons
        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();

                for (DataSnapshot child : children){
                    fetchedcartoons.add(child.getValue(Cartoon.class));
                }
                adapter= new ListAdapter(fetchedcartoons, MainActivity.this);
                cartoonRecycler.setAdapter(adapter);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
