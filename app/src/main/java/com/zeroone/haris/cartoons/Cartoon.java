package com.zeroone.haris.cartoons;

import java.util.ArrayList;
import java.util.Vector;

public class Cartoon {
    private String name;
    private String image;


    private ArrayList<YoutubeVideo> episodes;


    public Cartoon(){
        name="";
        image="";
        episodes = new ArrayList<>();
    }

    public Cartoon(String name, String image) {
        this.name = name;
        this.image=image;
        this.episodes= new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<YoutubeVideo> getEpisodes() {
        return episodes;
    }

    public void setEpisodes(ArrayList<YoutubeVideo> episodes) {
        this.episodes = episodes;
    }

    public String getImage() {return image; }

    public void setImage(String image) { this.image = image; }
}
