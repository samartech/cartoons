package com.zeroone.haris.cartoons;

import java.util.ArrayList;

public class YoutubeVideo {
    String videoUrl;

    public YoutubeVideo() {
    }

    public YoutubeVideo(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getImage(){
        String[] result = videoUrl.split("/");
        String imageurl = "https://img.youtube.com/vi/"+result[result.length-1]+"/0.jpg";
        return imageurl;
    }

    public String getID(){
        String[] result = videoUrl.split("/");
        return result[result.length-1];
    }
}