package com.zeroone.haris.cartoons;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.youtube.player.YouTubePlayerView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class EpisodeListAdapter extends RecyclerView.Adapter<EpisodeListAdapter.ViewHolder>{
    private static final String TAG = "EpisodeListAdapter";

    private ArrayList<YoutubeVideo> episodes;
    private Context context;

    public EpisodeListAdapter(ArrayList<YoutubeVideo> cartoons, Context context) {
        this.episodes = cartoons;
        this.context = context;
    }

    @NonNull
    @Override
    public EpisodeListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.episodelistitem, parent, false);
        ViewHolder viewholder = new ViewHolder(view);
       /* WebView videoView = parent.findViewById(R.id.videoView);
        videoView.loadData( "<html><body><iframe width=\"99%\" height=\"315\" src=\""+episodes.get(0).getVideoUrl()+"\" frameborder=\"0\" allowfullscreen></iframe></body></html>",
                "text/html" , "utf-8" );*/

        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull final EpisodeListAdapter.ViewHolder holder, final int position) {
        holder.episodelistText.setText("Episode "+(position+1));
        Picasso.get()
                .load(episodes.get(position).getImage())
                .resize(180,120)
                .centerCrop()
                .into(holder.episodelistImage);



        holder.episodeparent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO: reset content of webview to youtube url
                ((VideoActivity3)((Activity)context)).mYoutubePlayer.cueVideo(episodes.get(position).getID());
            }
        });
    }

    @Override
    public int getItemCount() {
        return episodes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView episodelistImage;
        TextView episodelistText;
        RelativeLayout episodeparent;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            episodelistImage = itemView.findViewById(R.id.episodelistImage);
            episodelistText = itemView.findViewById(R.id.episodelistText);
            episodeparent  = itemView.findViewById(R.id.episodelistparent);

        }
    }
}
