package com.zeroone.haris.cartoons;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.Image;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder>{
    private static final String TAG = "ListAdapter";

    private ArrayList<Cartoon> cartoons;
    private Context context;
    private ArrayList<String> colorsarray;

    public ListAdapter(ArrayList<Cartoon> cartoons, Context context) {
        this.cartoons = cartoons;
        this.context = context;
        colorsarray = new ArrayList<>(Arrays.asList( "#ef9a9a","#CE93D8","#9FA8DA","#81D4FA","#80CBC4","#C5E1A5","#FFF59D","#FFCC80","#B39DDB","#80DEEA"));
    }

    @NonNull
    @Override
    public ListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem, parent, false);
        ViewHolder viewholder = new ViewHolder(view);

        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull ListAdapter.ViewHolder holder, final int position) {
        holder.listText.setText(cartoons.get(position).getName().toUpperCase());
        Picasso.get()
                .load(cartoons.get(position).getImage())
                .resize(200,100)
                .centerCrop()
                .into(holder.listImage);


        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context,cartoons.get(position).getName(),Toast.LENGTH_SHORT ).show();
                Intent videoIntent = new Intent(context, VideoActivity3.class);
                videoIntent.putExtra("name",cartoons.get(position).getName());
                context.startActivity(videoIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return cartoons.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView listImage;
        TextView listText;

        RelativeLayout parent;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            listImage = itemView.findViewById(R.id.listImage);
            listText = itemView.findViewById(R.id.listText);
            parent  = itemView.findViewById(R.id.listParent);

        }
    }
}
